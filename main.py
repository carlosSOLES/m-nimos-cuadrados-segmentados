import matplotlib.pyplot as plt
import numpy as np
from plotnine import *
from plotnine.animation import PlotnineAnimation
import pandas as pd
import os


# Total de puntos
N = int(input("Ingrese el número de puntos: "))
X = np.arange(N)
Y = np.zeros(N)

# Arreglo que contiene las pendientes
M = []
# Arreglo que contiene las ordenadas
B = []

# numero real de lineas
L = int(input("Ingrese un número de lineas a generar: "))

# Desviacion estandar del ruido
sd = 4

d = int(np.ceil(N / L))

# Define los puntos de la grafica
ultimo = np.random.randint(-50, 50)
for l in range(L):
    m = np.random.randint(-50, 50)
    b = ultimo
    M.append(m)
    B.append(b)
    print([d*l, (l+1)*d])
    y = np.arange(len(X[d*l : (l+1)*d])) * m + b + (np.random.randn(len(X[d*l : (l+1)*d])) * sd)
    Y[d*l : (l+1)*d] = y
    ultimo = y[-1]
    print("Pendiente: ", m, "Bias: ", b)
    #print(y)


# Considera dos índices y calcula los valores estimados de 
def regresion(n1, n2, x, y):
    X = x[n1:n2+1]
    Y = y[n1:n2+1]
    # Usando la proporcion de covarianza
    m_hat = np.corrcoef(X,Y)[0,1] * np.std(Y) / np.std(X)
    b_hat = np.mean(Y) - m * np.mean(X)

    y_hat = X * m_hat + b_hat

    error = np.sum(Y-y_hat)**2
    
    #print(m_hat)
    #print(b_hat)
    #print(error)
    return m_hat, b_hat, error



C = float(input("Ingrese un valor de costo: "))

regresion(0, 20, X, Y)
err = np.zeros((N, N))

M = np.zeros(N)


M[0] = 0

for i in range(N):
    for j in range(i + 1, N):
        m, b, e = regresion(i, j, X, Y)
        #print("i: ", i, "j: ",  j, "m: ", e)
        err[i, j] = e


def reg(C):
    S = {}
    S["0"] = [0]
    for j in range(1, N):
        min = 10000000
        s_aux = [0]
        for i in range(1, j):
            m = err[i,j] + C + M[i-1]
            if m < min:
                min = m
                s_aux = S[str(i)].copy()
                s_aux.append(j)          
        M[j] = min
        S[str(j)] = s_aux
    return S[str(N-1)], S

S = reg(C)

print("Secuencia optima encontrada: ", S[0])




plt.scatter(X, Y)
plt.show()


#%%
S = reg(C)
l = np.max([len(S[1][s]) for  s in S[1]])
def plot(S, l):

    X1 = np.arange(N+l)
    Y1 = np.zeros(N+l)
    X1[0:N] = X
    Y1[0:N] = Y



    #C = np.tile("0", len(X))
    C = np.zeros_like(X1)
    
    
    # Total de puntos de quiebre
    t = len(S)

    # Etiquetar por segmentos
    if t > 1:
        for i in range(1, t):
            C[S[i-1]: S[i]+1] = i
            
    for i in range(l):
        if i not in C:
            C[-(i+1)] = i 


    df = pd.DataFrame({
        'X': X1,
        'Y': Y1,
        'C': pd.Series(C, dtype="category")
    })

    p = (ggplot(df, aes('X', 'Y', color='C'))
         + geom_point()
         + stat_smooth(method='lm')
         + lims(
             # All the plots have scales with the same limits
             color=(0, l-1),
             x=(0,N-1)
         )
         + scale_color_discrete()
         + theme_bw()
         #+ theme(
         #    aspect_ratio=1,
         #)
    )
    return p

p = plot(S[0], l)
print(p)

plots = (plot(S[1][s], l) for s in S[1])

ani = PlotnineAnimation(plots, interval=200, repeat_delay=2000)
ani.save('animation.mp4')
os.system("mplayer -idle -fixed-vo -vo gl animation.mp4")
